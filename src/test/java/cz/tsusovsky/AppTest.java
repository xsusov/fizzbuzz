package cz.tsusovsky;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * simple {@link App} integration test
 * @author tsusovsky
 * @date 08/25/2020
 */
public class AppTest {
    // App wrapper under test
    private App app;

    @Test
    public void test() throws Exception {
        FizzBuzzer fizzBuzzer = new FizzBuzzer();
        OutputStream testOut = new ByteArrayOutputStream(512);
        PrintStream out = new PrintStream(testOut);

        app = new App(1, 100, fizzBuzzer, out);
        app.run();
        // App output shall be recorded in testOut
        List<String> lines = testOut.toString().lines().collect(Collectors.toList());

        Assert.assertEquals(100, lines.size());
        int expectedFizzBuzzesCount = 100 / 15; // 15 minimal common multiple
        Assert.assertEquals(100 / 5 - expectedFizzBuzzesCount, lines.stream().filter(l -> "Agile".equals(l)).count());
        Assert.assertEquals(100 / 3 - expectedFizzBuzzesCount, lines.stream().filter(l -> "Software".equals(l)).count());
        Assert.assertEquals(expectedFizzBuzzesCount, lines.stream().filter(l -> "Testing".equals(l)).count());
    }

}
