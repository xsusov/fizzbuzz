package cz.tsusovsky;

import org.junit.Assert;
import org.junit.Test;

/**
 * {@link FizzBuzzer} unit test
 * @author tsusovsky
 * @date 08/25/2020
 */
public class FizzBuzzerTest {
    // fizzbuzzer under test
    private FizzBuzzer fizzBuzzer = new FizzBuzzer();

    @Test
    public void fizz() {
        Assert.assertEquals("Agile", fizzBuzzer.fizzbuzz(5));
    }

    @Test
    public void buzz() {
        Assert.assertEquals("Software", fizzBuzzer.fizzbuzz(3));
    }

    @Test
    public void fizzbuzz() {
        Assert.assertEquals("Testing", fizzBuzzer.fizzbuzz(15));
    }

    @Test
    public void noMatch() {
        Assert.assertEquals("2", fizzBuzzer.fizzbuzz(2));
    }

    @Test
    public void configurable() {
        FizzBuzzer configurableFizzBuzzer = new FizzBuzzer(7, 9);
        Assert.assertEquals("Agile", configurableFizzBuzzer.fizzbuzz(7));
        Assert.assertEquals("Software", configurableFizzBuzzer.fizzbuzz(9));
        Assert.assertEquals("Testing", configurableFizzBuzzer.fizzbuzz(63));
        Assert.assertEquals("2", configurableFizzBuzzer.fizzbuzz(2));
    }

}
