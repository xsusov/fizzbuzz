package cz.tsusovsky;

/**
 * Simple numeric value mapper to category label based on simple rules.
 * @author tsusovsky
 * @date 08/25/2020
 */
public class FizzBuzzer {
    private static String FIZZ = "Agile";
    private static String BUZZ = "Software";
    private static String FIZZBUZZ = "Testing";
    
    private int fizzModule;
    private int buzzModule;

    /**
     * Configurable FizzBuzzer CTOR
     * @param fizzModule - module for Fizz category check
     * @param buzzModule - module for Buzz category check
     */
    public FizzBuzzer(int fizzModule, int buzzModule) {
        this.fizzModule = fizzModule;
        this.buzzModule = buzzModule;
    }

    /**
     * Default FizzBuzzer CTOR ()
     */
    public FizzBuzzer() {
        this(5, 3);
    }

    /**
     * Maps number to FIZZ or BUZZ string when matching one category, or to FIZZBUZZ string when matching both of them.
     * Numbers not matching any value are just converted to string representation.
     * @param n - input number
     * @return category name or sting representation of number
     */
    public String fizzbuzz(int n) {
        boolean isFizz = isFizz(n);
        boolean isBuzz = isBuzz(n);
        
        if (isFizz && isBuzz) {
            return FIZZBUZZ;
        } else if (isFizz) {
            return FIZZ;
        } else if (isBuzz) {
            return BUZZ;
        } else {
            return String.valueOf(n);
        }
    }

    private boolean isFizz(int n) {
        return n % fizzModule == 0;
    }

    private boolean isBuzz(int n) {
        return n % buzzModule == 0;
    }

}
