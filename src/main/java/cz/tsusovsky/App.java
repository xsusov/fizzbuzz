package cz.tsusovsky;

import java.io.PrintStream;
import java.util.Collections;
import java.util.stream.IntStream;

/**
 * Demo Application printing range of fizzbuzzed numbers.
 * @author tsusovsky
 * @date 08/25/2020
 */
public class App {
    private final FizzBuzzer fizzBuzzer;
    private final PrintStream out;
    private final int from;
    private final int to;

    App(int from, int to, FizzBuzzer fizzBuzzer, PrintStream out) {
        this.from = from;
        this.to = to;
        this.fizzBuzzer = fizzBuzzer;
        this.out = out;
    }

    public static void main(String[] argv) {
        try {
            FizzBuzzer fizzBuzzer = new FizzBuzzer();
            PrintStream out = System.out;
            App app = new App(1, 100, fizzBuzzer, out);
            app.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.exit(0);
    }

    public void run() {
        IntStream.rangeClosed(from, to)
                .boxed()
                .sorted(Collections.reverseOrder())
                .map(fizzBuzzer::fizzbuzz)
                .forEach(out::println);
    }
}
